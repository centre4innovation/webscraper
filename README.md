# HxScrape #

Web scraping service.

Humanity-x.org - Centre for Innovation - Leiden University

## Getting started ##

* Install Java 1.8+
* Run maven build
* In the target directory you'll find: `scrape-x.y.z.jar`
* Run: java -jar scrape-x.y.z.jar server ../settings.yml
* Browse to: http://localhost:1337/


