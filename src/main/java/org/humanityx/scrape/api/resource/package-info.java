/**
 * Model the resources exposed in your REST-ful API
 */
package org.humanityx.scrape.api.resource;